resource "aws_iam_role_policy" "ansible_policy" {
  name = "${var.stackname}-ansible_policy"
  role = "${aws_iam_role.ansible_role.name}"
  policy = <<EOF
{
  "Version": "2012-10-17",
  "Statement": [
    {
      "Action": [
        "ec2:Describe*"
      ],
      "Effect": "Allow",
      "Resource": "*"
    },
    {
      "Action": [
        "s3:*"
      ],
      "Effect": "Allow",
      "Resource": "*"
    }
    ]
}
EOF
}

resource "aws_iam_role" "ansible_role" {
  name = "${var.stackname}-ansible_role"
  assume_role_policy = <<EOF
{
  "Version": "2012-10-17",
  "Statement": [
    {
      "Action": "sts:AssumeRole",
      "Principal": {
        "Service": "ec2.amazonaws.com"
      },
      "Effect": "Allow",
      "Sid": ""
    }
  ]
}
EOF
}

resource "aws_iam_instance_profile" "ansible_profile" {
  name = "${var.stackname}-ansible_profile"
  role = "${aws_iam_role.ansible_role.name}"
}
