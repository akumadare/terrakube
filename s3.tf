resource "aws_s3_bucket" "ansible_bucket" {
  acl    = "private"
  tags {
    Name        = "${var.stackname}-ansible_bucket"
  }
}

resource "aws_s3_bucket_object" "ansible_zip_s3" {
  bucket = "${aws_s3_bucket.ansible_bucket.bucket}"
  key    = "ansible.zip"
  source = "${data.archive_file.ansible_zip.output_path}"
  etag   = "${md5(file("${data.archive_file.ansible_zip.output_path}"))}"
  depends_on = ["data.archive_file.ansible_zip"]
}

data "archive_file" "ansible_zip" {
  type        = "zip"
  source_dir  = "files/ansible"
  output_path = "files/ansible.zip"
}

