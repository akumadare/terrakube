resource "aws_route_table" "tform_publicroutetable" {
  vpc_id = "${aws_vpc.vpc.id}"
  route {
    cidr_block = "0.0.0.0/0"
    gateway_id = "${aws_internet_gateway.tform_igw.id}"
  }
  tags {
    Name = "${var.stackname}_publicroutetable"
  }
}
resource "aws_route_table" "tform_privateroutetable" {
  vpc_id = "${aws_vpc.vpc.id}"
  route {
    cidr_block = "0.0.0.0/0"
    gateway_id = "${aws_nat_gateway.gw.id}"
  }
  tags {
    Name = "${var.stackname}_privateroutetable"
  }
}