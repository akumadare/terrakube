resource "aws_instance" "bastion" {
  ami           = "${var.ami1}"
  instance_type = "t2.micro"
  subnet_id = "${aws_subnet.publicsubnet1.id}"
  root_block_device {
    delete_on_termination = true
    volume_type           = "gp2"
  }
  key_name = "${var.stackname}-key"
  vpc_security_group_ids = [ "${aws_security_group.sg_sshin.id}", "${aws_security_group.sg_bastion.id}" ]
  tags {
    Name = "${var.stackname}-bastion"
  }
}