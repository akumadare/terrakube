resource "aws_vpc" "vpc" {
  cidr_block = "10.0.0.0/16"
  tags {
    Name = "${var.stackname}_vpc"
  }
}
resource "aws_internet_gateway" "tform_igw" {
  vpc_id = "${aws_vpc.vpc.id}"
  tags {
    Name = "${var.stackname}_igw"
  }
}