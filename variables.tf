variable "stackname" {
  type = "string"
  default = "kubernetes5"
}
variable "environment" {
  type = "string"
  default = "test1"
}
variable "nodecount" {
  type = "string"
  default = "2"
}
variable "instance_type" {
  type = "string"
  default = "t2.medium"
}
variable "ami1" {
  type = "string"
  default = "ami-9887c6e7"
}

