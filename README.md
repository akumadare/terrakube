# Kubernetes Cluster Build
xxxx

This repository contains the scripts and cdoe required to create a Kubernetes cluster in AWS. 

Terraform is used to provision the infrastructure including:
* VPC / Subnets / Route tables / Security groups
* Kubernetes master and nodes
* SSH Bastion server for access to private subnet resources
* Ansible server

The Ansible server is used to configure the Kubernetes nodes:
* Configure OS for Kubernetes
* Install docker and Kubernetes components
* Create Kubernetes master
* Join Kubernetes nodes to the master
* Add container networking interface

## Prerequisites

1. Terraform installed https://www.terraform.io/
2. Git client installed including git bash https://gitforwindows.org/

## Stack launch procedure

The following are instructions for launching this stack within a bash session (git bash)

1. Clone this repository and change into the directory created
```
git clone https://github.com/akumadare/terraform.git && cd terraform
```
2. Set up the AWS credentials for a user with admin role
```
$ export AWS_ACCESS_KEY_ID=AKIAIOSFODNN7EXAMPLE
$ export AWS_SECRET_ACCESS_KEY=wJalrXUtnFEMI/K7MDENG/bPxRfiCYEXAMPLEKEY
$ export AWS_DEFAULT_REGION=us-west-2
```
3. edit the *variables.tf* file variables - these are described below:

  * stackname : an identifier for the stack - this will be used to prefix names and some other tags to identify resources created by this stack
  * environment : an indicator of the environment type: live, development, test, uat etc - will be used for tagging
  * nodecount : this indicates the number of kubernetes worker nodes that will be created

4. Initial terraform to download required plugins
```
terraform init
```
4. Create the stack with terraform
```
terraform apply
```

## Connecting to Ansible

1. Set up ssh-agent
```
eval `ssh-agent`
```
2. Add the private key for the stack
```
ssh-add id_rsa
```
3. Connect to the bastion instance (all the other instances are in private subnets) - the terraform output should detail the address
```
ssh -A centos@[bastion public ip]
```
4. Connect from the bastion instance to Ansible - the terraform output should detail the address
```
ssh -A centos@[ansible private ip]
```

## Testing control over the Ansible clients

1. sudo to *root* and preserve the ssh-agent socket
```
sudo -s -E
```
2. Change to the Ansible directory
```
cd /etc/ansible
```
3. Test Ansible control - this Ansible pings all Ansible_managed=True tagged instances and should return a *SUCCESS*
```
ansible tag_Ansible_managed_True -m ping -i ec2.py -u centos
```

## Installing kubernetes on the nodes

1. Run the ansible playbook
```
cd /etc/ansible && ansible-playbook kubernetessetup.yml -i ec2.py
```

## Tips

* Use *sudo -E* when switching across to root on the Ansible server to preserve ssh-agent socket
