resource "aws_key_pair" "key" {
  key_name   = "${var.stackname}-key"
  public_key = "${tls_private_key.stack_key.public_key_openssh}"
}

resource "tls_private_key" "stack_key" {
  algorithm   = "RSA"
}
resource "local_file" "stack_key_priv_file" {
    content     = "${tls_private_key.stack_key.private_key_pem}"
    filename = "${path.module}/${var.stackname}_id_rsa"
}
resource "local_file" "stack_key_pub_file" {
    content     = "${tls_private_key.stack_key.public_key_openssh}"
    filename = "${path.module}/${var.stackname}_id_rsa.pub"
}
