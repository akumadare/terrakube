output "connecting" {
  value = "Connect to the bastion ${aws_instance.bastion.public_ip}\nConnect on the Ansible server ${aws_instance.ansible-instance.private_ip}"
}
