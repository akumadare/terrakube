resource "aws_instance" "ansible-instance" {
  ami           = "${var.ami1}"
  instance_type = "t2.micro"
  subnet_id = "${aws_subnet.managementsubnet.id}"
  key_name = "${var.stackname}-key"
  vpc_security_group_ids = [ "${aws_security_group.sg_accessfrombastion.id}", "${aws_security_group.private_default.id}", "${aws_security_group.sg_sshin.id}"]
  iam_instance_profile= "${aws_iam_instance_profile.ansible_profile.name}"
  root_block_device {
    delete_on_termination = true
    volume_type           = "gp2"
  }
  tags {
    Name = "${var.stackname}-ansible"
    Type = "ansible"
  }
  user_data = "${data.template_file.ansible_userdata.rendered}"
}
data "template_file" "ansible_userdata" {
  template = "${file("ansible_userdata.tpl")}"
  vars {
    bucket = "${aws_s3_bucket.ansible_bucket.bucket}"
  }
}
