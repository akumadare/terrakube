resource "aws_security_group" "sg_sshin" {
  name        = "${var.stackname}_sshin"
  description = "Allow ssh inbound traffic"
  vpc_id      = "${aws_vpc.vpc.id}"
  ingress {
    from_port   = 22
    to_port     = 22
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }
  egress {
    from_port       = 0
    to_port         = 0
    protocol        = "-1"
    cidr_blocks     = ["0.0.0.0/0"]
  }
}
resource "aws_security_group" "sg_bastion" {
  name        = "${var.stackname}_bastion"
  description = "SG for Bastion host"
  vpc_id      = "${aws_vpc.vpc.id}"
}
resource "aws_security_group" "sg_accessfrombastion" {
  name        = "${var.stackname}_accessfrombastion"
  description = "SG for access from Bastion host"
  vpc_id      = "${aws_vpc.vpc.id}"
  ingress {
    from_port   = 22
    to_port     = 22
    protocol    = "tcp"
    security_groups = ["${aws_security_group.sg_bastion.id}"]
  }
}
resource "aws_security_group" "private_default" {
  name        = "${var.stackname}_private_default"
  description = "Allow outbound traffic"
  vpc_id      = "${aws_vpc.vpc.id}"
  egress {
    from_port       = 0
    to_port         = 0
    protocol        = "-1"
    cidr_blocks     = ["0.0.0.0/0"]
  }
}
resource "aws_security_group" "sg_accessfromansible" {
  name        = "${var.stackname}_accessfromansible"
  description = "SG for access from ansible server"
  vpc_id      = "${aws_vpc.vpc.id}"
  ingress {
    from_port   = 22
    to_port     = 22
    protocol    = "tcp"
    cidr_blocks = ["${aws_instance.ansible-instance.private_ip}/32"]
  }
}
resource "aws_security_group" "sg_kubernetes_cluster" {
  name        = "${var.stackname}_kubernetes_cluster"
  description = "SG for access within the kubernetes cluster"
  vpc_id      = "${aws_vpc.vpc.id}"
  ingress {
    from_port   = 0
    to_port     = 0
    protocol    = -1
    self = true
  }
}