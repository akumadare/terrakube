#!/bin/bash

# create master node locally
kubeadm init --pod-network-cidr=192.168.0.0/16 >> /tmp/kubeadm.out

# set up the admin config in the bash profile and current environment
export KUBECONFIG=/etc/kubernetes/admin.conf
cat << 'EOF' > /root/.bash_profile
export KUBECONFIG=/etc/kubernetes/admin.conf
EOF

# get the cluster join URL for the worker nodes - run this on each worker
grep 'kubeadm join' /tmp/kubeadm.out > /tmp/kubejoin.out

# install calico networking overlay
kubectl apply -f https://docs.projectcalico.org/v3.1/getting-started/kubernetes/installation/hosted/rbac-kdd.yaml
kubectl apply -f https://docs.projectcalico.org/v3.1/getting-started/kubernetes/installation/hosted/kubernetes-datastore/calico-networking/1.7/calico.yaml

# check node status for the cluster
kubectl get nodes

