#!/bin/bash

# turn off swap
swapoff -a

# comment out swap line from fstab
sed -i.bak 's/\(.*swap.*\)/#\1/' /etc/fstab

# set up kubernetes repo file
cat << 'EOF' > /etc/yum.repos.d/kubernetes.repo
[kubernetes]
name=Kubernetes
baseurl=https://packages.cloud.google.com/yum/repos/kubernetes-el7-x86_64
enabled=1
gpgcheck=1
repo_gpgcheck=1
gpgkey=https://packages.cloud.google.com/yum/doc/yum-key.gpg https://packages.cloud.google.com/yum/doc/rpm-package-key.gpg
EOF

# set up k8s sysctl config
cat << 'EOF' > /etc/sysctl.d/k8s.conf
net.bridge.bridge-nf-call-ip6tables = 1
net.bridge.bridge-nf-call-iptables = 1
EOF

# setup docker repo, install and enable
yum-config-manager --add-repo https://download.docker.com/linux/centos/docker-ce.repo
yum -y install docker
systemctl enable docker
systemctl restart docker

# configure selinux to be permissive
setenforce 0 && sed -i.bak 's/^SELINUX=.*/SELINUX=permissive/g' /etc/selinux/config

# update sysctl settings
sysctl --system

# install kubernetes components
yum -y install kubectl kubeadm kubelet

