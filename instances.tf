resource "aws_instance" "kubernetesmaster" {
  ami           = "${var.ami1}"
  instance_type = "${var.instance_type}"
  subnet_id = "${aws_subnet.managementsubnet.id}"
  key_name = "${var.stackname}-key"
  root_block_device {
    delete_on_termination = true
    volume_type           = "gp2"
  }
  vpc_security_group_ids = [ "${aws_security_group.sg_accessfrombastion.id}", "${aws_security_group.private_default.id}", "${aws_security_group.sg_kubernetes_cluster.id}", "${aws_security_group.sg_accessfromansible.id}"]
  tags {
    Name = "${var.stackname}_kubernetesmaster"
    Type = "kubernetes"
	SubType = "kubernetes_master"
	Ansible_managed = "True"
  }
}
resource "aws_instance" "kubernetesnode" {
  ami           = "${var.ami1}"
  count = "${var.nodecount}"
  instance_type = "${var.instance_type}"
  subnet_id = "${element(data.aws_subnet_ids.kubernetes.ids, count.index)}"
  root_block_device {
    delete_on_termination = true
    volume_type           = "gp2"
  }
  key_name = "${var.stackname}-key"
  vpc_security_group_ids = [ "${aws_security_group.sg_accessfrombastion.id}", "${aws_security_group.private_default.id}", "${aws_security_group.sg_kubernetes_cluster.id}", "${aws_security_group.sg_accessfromansible.id}"]
  tags {
    Name = "${var.stackname}_kubernetesnode${count.index}"
    Type = "kubernetes"
	SubType = "kubernetes_node"
	Ansible_managed = "True"
  }
}


