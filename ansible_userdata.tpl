#!/bin/bash

# install epel repo
yum -y install https://dl.fedoraproject.org/pub/epel/epel-release-latest-7.noarch.rpm

# install ansible and pip
yum -y install ansible python-pip git unzip

# edit ansible.cfg file to set ansible as the remote user for connections
sed -i.bak '/remote_user/d; /host_key_checking/d;  /\[defaults\]/a host_key_checking = False\nremote_user = centos' /etc/ansible/ansible.cfg

# generate an ssh key for root user - ansible will run as this user
ssh-keygen -t RSA -N "" -f /root/.ssh/id_rsa

# install the python dependencies
pip install boto3 awscli boto

# get the helper script and ini file
cd /etc/ansible && curl -O https://raw.githubusercontent.com/ansible/ansible/devel/contrib/inventory/ec2.py
#cd /etc/ansible && curl -O https://raw.githubusercontent.com/ansible/ansible/devel/contrib/inventory/ec2.ini
chmod u+x /etc/ansible/ec2.py

# set up environment variables in the root profile
cat << EOF >> /root/.bashrc
export ANSIBLE_HOSTS=/etc/ansible/ec2.py
export EC2_INI_PATH=/etc/ansible/ec2.ini
EOF

# fetch the ec2.ini file from the S3 bucket
aws s3 cp s3://${bucket}/ansible.zip /tmp/ansible.zip

# extract the ansible zip
unzip -o -d /etc/ansible /tmp/ansible.zip










