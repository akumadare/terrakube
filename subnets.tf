resource "aws_subnet" "publicsubnet1" {
  vpc_id     = "${aws_vpc.vpc.id}"
  availability_zone = "us-east-1a"
  cidr_block = "10.0.1.0/24"
  map_public_ip_on_launch = true
  tags {
    Name = "${var.stackname}_publicsubnet1"
  }
}
resource "aws_subnet" "publicsubnet2" {
  vpc_id     = "${aws_vpc.vpc.id}"
  availability_zone = "us-east-1b"
  cidr_block = "10.0.2.0/24"
  map_public_ip_on_launch = true
  tags {
    Name = "${var.stackname}_publicsubnet2"
  }
}
resource "aws_subnet" "managementsubnet" {
  vpc_id     = "${aws_vpc.vpc.id}"
  availability_zone = "us-east-1a"
  cidr_block = "10.0.5.0/24"
  tags {
    Name = "${var.stackname}_managementsubnet"
  }
}
resource "aws_subnet" "privatesubnet1" {
  vpc_id     = "${aws_vpc.vpc.id}"
  availability_zone = "us-east-1a"
  cidr_block = "10.0.3.0/24"
  tags {
    Name = "${var.stackname}_privatesubnet1"
    Kubernetes = true
  }
}
resource "aws_subnet" "privatesubnet2" {
  vpc_id     = "${aws_vpc.vpc.id}"
  availability_zone = "us-east-1b"
  cidr_block = "10.0.4.0/24"
  tags {
    Name = "${var.stackname}_privatesubnet2"
    Kubernetes = true
  }
}
resource "aws_route_table_association" "publicrta1" {
  subnet_id      = "${aws_subnet.publicsubnet1.id}"
  route_table_id = "${aws_route_table.tform_publicroutetable.id}"
}
resource "aws_route_table_association" "publicrta2" {
  subnet_id      = "${aws_subnet.publicsubnet2.id}"
  route_table_id = "${aws_route_table.tform_publicroutetable.id}"
}
resource "aws_route_table_association" "privaterta1" {
  subnet_id      = "${aws_subnet.privatesubnet1.id}"
  route_table_id = "${aws_route_table.tform_privateroutetable.id}"
}
resource "aws_route_table_association" "privaterta2" {
  subnet_id      = "${aws_subnet.privatesubnet2.id}"
  route_table_id = "${aws_route_table.tform_privateroutetable.id}"
}
resource "aws_route_table_association" "privaterta3" {
  subnet_id      = "${aws_subnet.managementsubnet.id}"
  route_table_id = "${aws_route_table.tform_privateroutetable.id}"
}
data "aws_subnet_ids" "kubernetes" {
  vpc_id = "${aws_vpc.vpc.id}"
  depends_on = ["aws_subnet.privatesubnet2","aws_subnet.privatesubnet2"]
  tags {
    Kubernetes = true
  }
}